import { Db } from "./db.js"


function criarCliente(dados) {
    return Db.create(dados, "tabelaPessoaFisica")
}

function buscarCientes() {
    return Db.find("tabelaPessoaFisica")
}
